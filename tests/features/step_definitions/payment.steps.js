import {When,Then, Given, And} from 'cucumber'
import addNewPayment from '../../pages/paymentPages/addNewPayment.page';

When("I fill with valid {string} card info", (arg1) => {
    addNewPayment.fillWithValidCardData(arg1);
});

When("I fill with invalid card info", () => {
    addNewPayment.fillWithInvalidCardData();
});

Then("I should be able to see all card warning messages", () => {
    expect(addNewPayment.cardHolderWarningField).toHaveText('This field may only contain alphabetic characters as well as spaces.');
    expect(addNewPayment.cardNumberWarningField).toHaveText('This field must be at least 15 characters.')
    expect(addNewPayment.expirationDateWarningField).toHaveText('Expiration Date is not valid.')
    expect(addNewPayment.cvcNumberWarningField).toHaveText('This field must be at least 3 characters.')
    
});

When("I fill with wrong card number", () => {
    addNewPayment.fillWithWrongCardNumber();
});

When("I set as a default credit card", () => {
    addNewPayment.clickSetAsADefault();
});

When("I delete a payment method", () => {
    addNewPayment.deletePaymentMethod();
});

Then("I should be able to see {string} or {string} notification", (arg1,arg2) => {
    addNewPayment.verificationMessage(arg1,arg2);
});
