import {
    Given,
    When,
    Then
} from 'cucumber'
import projectsHomePage from '../../pages/projectPages/projectsHome.page';
import createNewProjectPage from '../../pages/projectPages/createNewProject.page';
import uploadDocumentPage from '../../pages/projectPages/uploadDocument.page';
import selectLanguagePage from '../../pages/projectPages/selelctLanguage.page';
import documentDetailsPage from '../../pages/projectPages/documentDetails.page';
import certificationTypePage from '../../pages/projectPages/certificationType.page';
import translationDetailsPage from '../../pages/projectPages/translationDetails.page';
import legalAidPage from '../../pages/projectPages/legalAid.page';
import selectTranslatorPage from '../../pages/projectPages/selectTranslator.page';
import reviewProjectPage from '../../pages/projectPages/reviewProject.page';
import paymentDetailPage from '../../pages/projectPages/paymentDetail.page';
import projectConclusionPage from '../../pages/projectPages/projectConclusion.page';
import appointmentTypePage from '../../pages/projectPages/appointmentType.page';
import videoCallDetailsPage from '../../pages/projectPages/videoCallDetails.page';
import interpretationDetailsPage from '../../pages/projectPages/interpretationDetails.page';
import projectDetailsPage from '../../pages/projectPages/projectDetails.page';

When("I click plus symbol", () => {
    projectsHomePage.clickPlusSymbol();
});

When("I select {string} project", (arg1) => {
    createNewProjectPage.clickProject(arg1);
    createNewProjectPage.clickContinue();
});

When("I upload documents", () => {
    uploadDocumentPage.uploadDocAndContinue();
});

When("I upload invalid document", () => {
    uploadDocumentPage.uploadInvalidDocAndContinue();
});


When("I select language from {string} to {string}", (arg1, arg2) => {
    selectLanguagePage.selectLanguages(arg1, arg2);
    selectLanguagePage.clickContinue();
});

When("I select document type as a {string} and document details {string}", (arg1, arg2) => {
    documentDetailsPage.selectDocumentType(arg1, arg2)
    documentDetailsPage.clickContinue();
});

When("I select certification type number {string}", (arg1) => {
    certificationTypePage.selectCertification(arg1)
    certificationTypePage.clickContinue();
});

When("I type project title as a {string} and select date as a {string}", (arg1, arg2) => {
    translationDetailsPage.selectProjectTitleDueDate(arg1, arg2);
});

When("I click continue", () => {
    translationDetailsPage.clickContinue();
});

When("I select legal aid details {string}", (arg1) => {
    legalAidPage.selectLegalAid(arg1);
    legalAidPage.clickContinue();
});

When("I select translator", () => {
    selectTranslatorPage.selectTranslator();
    selectTranslatorPage.clickContinue();
});

When("I check details of projects", () => {
    reviewProjectPage.reviewClick();
});

When("I enter {string} referal code and select payment method", (arg) => {
    paymentDetailPage.enterReferalCode(arg);
    //paymentDetailPage.selectDefaultPaymentMethod();
    paymentDetailPage.clickContinue();
});

When("I enter {string} legal aid number and {string} date", (arg1, arg2) => {
    legalAidPage.enterLANumberAndExpireDate(arg1, arg2);
});

When("I select appointment type as {string} interpretation", (arg1) => {
    appointmentTypePage.selectAppointmentType(arg1);
});

When("I select start date {string}-{string}-{string} {string}:{string} and end date {string}-{string}-{string} {string}:{string}",  (arg1, arg2,arg3, arg4,arg5, arg6,arg7, arg8,arg9, arg10)=> {
    appointmentTypePage.selectAppointmentDuration(arg1, arg2,arg3, arg4,arg5, arg6,arg7, arg8,arg9, arg10);
    //appointmentTypePage.clickContinue();
});

When("I select video chat platform as {string} and contact details as {string}",(arg1, arg2)=> {
    videoCallDetailsPage.selectProgram(arg1);
    videoCallDetailsPage.enterContactInfo(arg2);
    videoCallDetailsPage.clickContinue();
});

When("I enter project title as {string}, appointment topic as {string} and appointment details as {string}", (arg1, arg2, arg3)=> {
    interpretationDetailsPage.enterInterpretationTitle(arg1);
    interpretationDetailsPage.selectAppointmentTopic(arg2);
    interpretationDetailsPage.selectAppointmentDetails(arg3);
});

When("I select the project", () => {
   projectsHomePage.goToFirstProject()
});

When("I click delete button", () => {
    projectDetailsPage.deleteProject();
});

Then("I should be on the {string} page", (arg) => {
    projectsHomePage.waitForWrapperLoadMaskDisappear()
    projectsHomePage.waitForPageLoad(arg)
    expect(projectsHomePage.pageTitle).toHaveText(arg)
});

Then("I should able to see project offer sent message", () => {
    projectConclusionPage.waitForPageLoad("Project Offers Sent!");
    expect(projectConclusionPage.pageTitle).toHaveText("Project Offers Sent!")
});

Then("I should be able to see title is required message", () => {
    expect(translationDetailsPage.titleWarningField).toHaveText("This field is required.")
});


Then("I should be able to see code can not be found message", () => {
    expect(paymentDetailPage.referalCodeError).toHaveTextContaining("Code cannot be found.")
});

Then("I should be able to see certification number already exist message", () => {
    expect(legalAidPage).toHaveText("LAO Certification with this Certification Number already exists.")
});

Then("I should be able to see appointment duration notice", () => {
    expect(appointmentTypePage.appointmentDurationNotice).toHaveText("Appointment duration cannot be less than 2 hours.")
});