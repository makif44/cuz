@regression
@project

Feature: Creating new project

    As a user I should be able to create new project

Background: I am on the project page
    Given I am on the login page
    When I login "user" email and "user" password

@createProject @smoke
Scenario: Create a document translation with valid data
    When I click plus symbol
    And  I select "translation" project
    And I upload documents
    And I select language from "Turkish" to "English"
    And I select document type as a "Legal" and document details "contracts"
    And I select certification type number "0"
    And I type project title as a "driver lisance" and select date as a "30.07.2020 - 12:00" 
    And I click continue
    And I select legal aid details "no"
    And I select translator
    And I check details of projects
    And I enter "valid" referal code and select payment method
    Then I should able to see project offer sent message

@createProjectWithInvalidUpload
Scenario: Create a document with invalid document
    When I click plus symbol
    And I select "translation" project
    And I upload invalid document
    Then I should be able to see "Warning:" red wrapper notification

@createProjectWithInvalidTitle @smoke
Scenario: Create a document with invalid title
    When I click plus symbol
    And  I select "translation" project
    And I upload documents
    And I select language from "Turkish" to "English"
    And I select document type as a "Legal" and document details "contracts"
    And I select certification type number "0"
    And I type project title as a " " and select date as a "30.07.2020 - 12:00"
    Then I should be able to see title is required message
@usedLAONumber
Scenario: Create a document with used LAO number
    When I click plus symbol
    And  I select "translation" project
    And I upload documents
    And I select language from "Turkish" to "English"
    And I select document type as a "Legal" and document details "contracts"
    And I select certification type number "0"
    And I type project title as a "driver lisance" and select date as a "30.07.2020 - 12:00" 
    And I click continue
    And I select legal aid details "yes"
    And I enter "used" legal aid number and "2020-07-31" date
    Then I should be able to see certification number already exist message

@invalidReferalCode @smoke
Scenario: Creating a document with invalid referal and promo code
    When I click plus symbol
    And  I select "translation" project
    And I upload documents
    And I select language from "Turkish" to "English"
    And I select document type as a "Legal" and document details "contracts"
    And I select certification type number "0"
    And I type project title as a "driver lisance" and select date as a "30.07.2020 - 12:00" 
    And I click continue
    And I select legal aid details "no"
    And I select translator
    And I check details of projects 
    And I enter "invalid" referal code and select payment method
    Then I should be able to see code can not be found message

@deleteProject 
Scenario: delete the first project
    When I select the project
    And I click delete button
    Then I should be on the "Projects" page




