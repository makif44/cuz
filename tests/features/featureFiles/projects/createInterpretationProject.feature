@regression
@interpretationProject
Feature: Creating interpretation project

    As a user I should be able to create a new interpretation project

Background: I am on the project page
    Given I am on the login page
    When I login "user" email and "user" password

@createInterpretationProject @smoke
Scenario: Create interpretation project with valid data
    When I click plus symbol
    And  I select "interpretation" project
    And I select language from "Turkish" to "English"
    And I select appointment type as "Video" interpretation
    And I select start date "2021"-"August"-"30" "10":"00" and end date "2021"-"August"-"30" "11":"00"
    And I click continue
    And I select video chat platform as "Skype" and contact details as "chat"
    And I enter project title as "test", appointment topic as "Legal" and appointment details as "Arbitration"
    And I click continue
    And I select translator
    And I check details of projects
    And I enter "valid" referal code and select payment method
    Then I should able to see project offer sent message

@createInterpretationProjectWithInvalidDuration
Scenario: Create interpretation project with invalid duration
    When I click plus symbol
    And  I select "interpretation" project
    And I select language from "Turkish" to "English"
    And I select appointment type as "On-site" interpretation
    And I select start date "2020"-"August"-"30" "10":"00" and end date "2020"-"August"-"30" "11":"00"
    Then I should be able to see appointment duration notice

@createInterpretationProjectWithInvalidTitle @smoke
Scenario: Create interpretation project with invalid title
    When I click plus symbol
    And  I select "interpretation" project
    And I select language from "Turkish" to "English"
    And I select appointment type as "Video" interpretation
    And I select start date "2020"-"August"-"30" "10":"00" and end date "2020"-"August"-"30" "11":"00"
    And I click continue
    And I select video chat platform as "Skype" and contact details as "chat"
    And I enter project title as " ", appointment topic as "Legal" and appointment details as "Arbitration"
    Then I should be able to see title is required message