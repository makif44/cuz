@regression
@smoke
@login
Feature: Sign UP Test Cases

Background: in login page
  Given  I am on the login page

  @loginPositive
  Scenario: login with valid credentials as a user
    When I login with "user" email adress
    And I login with "user" password
    Then I should be able to see "Projects" page title

  @loginPositive
  Scenario: login with valid credentials as a translator
    When I login with "translator" email adress
    And I login with "translator" password
    Then I should be able to see "Translator Registration" page title

  @loginNegative
  Scenario: login with invalid password
    When I login with "user" email adress
    And I login with "invalid" password
    Then I should be able to see "Password or email is wrong." message
 
  @invalidEmail
  Scenario: login with invalid email
    When I just login with "invalid" email adress 
    Then I should see invalid email error

  @invalidSigUp
  Scenario: login with invalid sign up code
    When I login with "user" email adress
    And sign up with verification code
    When I enter a invalid sign up code
    Then I should see that the code was wrong 



  
  
  
  
  
  
  # @login_with_credentials_ddt
  # Scenario Outline: DDT example with credentials, Login as <username>
  #   Given I am on the login page
  #   When I enter "translator" username and "translotor" password
  #   Then I should be able to see "Apply As A Translator" page title

  # @translator
  #   Examples:
  #     | translator           | translator |
  #     | translator4@test.com | 12345    |
  #     | translator3@test.com | 12345    |
  #     | translator2@test.com | 12345    |
  #     | translator1@test.com | 12345    |
    
     
     








