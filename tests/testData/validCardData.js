module.exports={

    'cardName':'DIMITRI OLYMPOS',
    'visaCardNumber': '4242424242424242',
    'americaCardNumber': '378282246310005',
    'wrongCardNumber':'1234123412341234',
    'expirationDate': '10/22',
    'cvc': '334',
    'a_cvc' : '1234',
    'country': 'Greece',
    'postalCode': '12543'
};