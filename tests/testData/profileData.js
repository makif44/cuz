module.exports={

    'name':'Dimitri',
    'lastName': 'Papadopulos',
    'phoneNumber': '6516458541',
    'country': 'Greece',
    'city': 'Athens',
    'street': 'Sofokleos',
    'postalCode': '12543',
    'referalCode': 'rrdmjgti',
    'usedLACnumber': '12312312321'
};