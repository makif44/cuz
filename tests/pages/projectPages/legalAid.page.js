import Page from "../base.page";
import data from '../../testData/profileData'

class legalAidPage extends Page {

    get yesCheck() {
        return $("//body/div[@id='__nuxt']/div[@id='__layout']/div[@class='section']/div/div[@class='legal-aid-details container w-container']/span/div[@class='component-wrapper']/div[1]/label[1]/span[1]")
    };
    get noCheck() {
        return $("//body/div[@id='__nuxt']/div[@id='__layout']/div[@class='section']/div/div[@class='legal-aid-details container w-container']/span/div[@class='component-wrapper']/div[2]/label[1]/span[1]")
    };
    get legalAidNumber() {
        return $("//input[@class='text-field w-input error']")
    };
    get expirationDate() {
        return $("//input[@class='dropdown-search with-icon calender w-input flatpickr-input active']")
    };


    selectLegalAid(arg1) {
        //this.waitForPageLoad("Legal Aid Details");
        //browser.pause(1000);
        //this.yesCheck.waitForDisplayed();
        browser.waitUntil(() =>
            this.noCheck.isClickable(), {
                timeout: 10000,
                timeoutMsg: "Button not clickable"
            });
        if (arg1 === "yes") {
            this.yesCheck.click();

        } else {
            this.noCheck.click();
        }
    }

    enterLANumberAndExpireDate(arg1, arg2) {
        if (arg1 === "used") {
            arg1 = data['usedLACnumber']
        };
        this.waitForWrapperLoadMaskDisappear();
        this.legalAidNumber.waitForDisplayed();
        this.legalAidNumber.click();
        this.legalAidNumber.setValue(arg1);
        this.expirationDate.click();
        this.expirationDate.setValue(arg2);
    }
}
export default new legalAidPage();