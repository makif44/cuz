import Page from "../base.page";

class certificationTypePage extends Page {

    get certification() {
        return $("(//span[@class='w-form-formradioinput w-form-formradioinput--inputType-custom radio-button'])[1]")
    }

    selectCertification() {
        //this.waitForWrapperLoadMaskDisappear();
        browser.waitUntil(()=>
        this.certification.isClickable(),
        {timeout:5000})
        // this.certification.waitForEnabled({
        //     timeout: 5000
        // });
        this.certification.click();
    }
}
export default new certificationTypePage