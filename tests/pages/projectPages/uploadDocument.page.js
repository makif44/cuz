import Page from "../base.page";
const path = require('path');

class uploadDocument extends Page {

    get dragDropField() {
        return $("//input[@class='file-input']")
    };
    get starUploadingButton() {
        return $("//div[@class='btn-wrapper']/button")
    }

    uploadDocAndContinue() {
        const filePath = "C:/Users/makif/Visual Studio Projects/WebdriverIO/cuz test/tests/testData/upload.pdf"
        this.dragDropField.waitForEnabled(5000);
        this.dragDropField.setValue(filePath);
        this.starUploadingButton.waitForClickable();
        this.starUploadingButton.click();
        this.countinueButton.waitForClickable({
            timeout: 60000
        });
        this.countinueButton.click();
    }

    uploadInvalidDocAndContinue() {
        const filePath = "C:/Users/makif/Visual Studio Projects/WebdriverIO/cuz test/tests/testData/invalidUpload.bat"
        this.dragDropField.waitForEnabled(5000);
        this.dragDropField.setValue(filePath);
        this.starUploadingButton.waitForClickable();
        this.starUploadingButton.click();
        this.countinueButton.waitForClickable({
            timeout: 50000
        });
        this.countinueButton.click();
    }

}
export default new uploadDocument();