import Page from "../base.page";

class translationDetailsPage extends Page{

    get countinueButton(){return $("//div[@class='bot-nav-column']/button")};
    get projectTitle(){return $("//input[@placeholder='Project Title']")};
    get dueDate(){return $("//input[@placeholder='Click to Select Your Due Date']")};
    get date(){return $("//body//span[37]")};
    get no(){return $("(//span[@class='w-form-formradioinput w-form-formradioinput--inputType-custom radio-button'])[2]")}
    get titleWarningField(){return $("(//p[@class='error-detail errors-block'])[1]")};
    
    selectProjectTitleDueDate(arg1,arg2){
        if (arg1===" ") {
            arg1="";
        }
        this.projectTitle.waitForEnabled(5000);
        this.projectTitle.setValue(arg1);
        this.dueDate.click();
        this.date.click();
        this.projectTitle.click();  
    }
}
export default new translationDetailsPage();