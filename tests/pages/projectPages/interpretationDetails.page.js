import Page from "../base.page";

class interpretationDetailsPage extends Page{
    get interpretationTitle(){
        return $("//input[@placeholder='Project Title']")
    }
    get appointmentTopic(){
        return $("(//div[@class='component-wrapper']//input)[2]");
    }
    get appointmentTopicList(){
        return $("(//ul[contains(@id,'listbox')])[1]")
    }
    get appointmentDetails(){
        return $("(//div[@class='component-wrapper']//input)[3]");
    }
    get appointmentDetailsList(){
        return $("(//ul[contains(@id,'listbox')])[2]");
    }

    enterInterpretationTitle(arg1){
        if (arg1===" ") {
            arg1="";
        }
        this.waitForWrapperLoadMaskDisappear();
        this.interpretationTitle.waitForEnabled({timeout:10000});
        this.interpretationTitle.setValue(arg1);
    }
    
    selectAppointmentTopic(arg1){
        this.appointmentTopic.setValue(arg1);
        browser.pause(250);
        this.appointmentTopicList.click();
    }

    selectAppointmentDetails(arg1){
        this.appointmentDetails.setValue(arg1);
        browser.pause(250);
        this.appointmentDetailsList.click();
    }

}
export default new interpretationDetailsPage();