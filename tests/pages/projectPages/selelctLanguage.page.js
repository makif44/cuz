import Page from "../base.page";

class selectLanguagePage extends Page{

    get countinueButton(){return $("//div[@class='bot-nav-column']/button")};
    get sourceLanguage(){return $("//input[@placeholder='Source Language']")};
    get targetLanguage(){return $("//input[@placeholder='Target Language']")};
    get sourceList(){return $("#vs1__listbox")};
    get targetList(){return $("((//ul[@role='listbox'])[2]/descendant::*)[1]")}
    
    selectLanguages(arg1,arg2){
        this.waitForWrapperLoadMaskDisappear();
        this.sourceLanguage.waitForEnabled(5000);
        this.sourceLanguage.click();
        this.sourceLanguage.setValue(arg1);
        browser.pause(250);
        this.sourceList.click();
        this.targetLanguage.click({x: 0, y: 0});
        this.targetLanguage.setValue(arg2);
        browser.pause(250);
        this.targetList.click();
        // this.countinueButton.waitForDisplayed(5000);
        // this.countinueButton.click();
        
    }
}
export default new selectLanguagePage();