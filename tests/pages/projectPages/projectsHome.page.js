import Page from '../base.page'

class ProjectsHomePage extends Page {
    get plusSymbol() {
        return $("(//button)[1]")
    };
    get pageTitle() {
        return $("//h2[@class='title']")
    };
    get firstProject() {
        return $("(//div[@class='project-title-column---list-item'])[1]")
    };


    clickProfileButton() {
        //browser.pause(8000);
        this.waitForWrapperLoadMaskDisappear();
        this.profileButton.waitForClickable({
            timeout: 15000
        });
        this.profileButton.click();
    }

    clickPlusSymbol() {
        this.waitForWrapperLoadMaskDisappear();
        browser.waitUntil(() =>
            this.plusSymbol.isClickable(), {
                timeout: 10000,
                timeoutMsg: "Button not clickable"
            });
        this.plusSymbol.click();

    }

    goToFirstProject() {
        this.waitForWrapperLoadMaskDisappear();
        browser.waitUntil(() =>
            this.firstProject.isDisplayed(), {
                timeout: 10000,
                timeoutMsg: "Button not clickable"
            });
        this.firstProject.click();
    }
   
}
export default new ProjectsHomePage();