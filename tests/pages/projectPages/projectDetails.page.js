import Page from "../base.page";

class projectDetailsPage extends Page{

    get deleteButton(){
        return $("//a[@class='delete-button w-inline-block']")
    }

    deleteProject() {
        this.waitForWrapperLoadMaskDisappear();
        this.deleteButton.waitForClickable({timeout:10000});
        this.deleteButton.click();
        this.confirmationYes.waitForDisplayed();
        this.confirmationYes.click();   
    }
}
export default new projectDetailsPage();