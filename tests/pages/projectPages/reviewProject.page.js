import Page from "../base.page";

class reviewProjectPage extends Page{

    get countinueButton(){return $("//button[@class='button w-button blue']")};
    
    reviewClick(){
        this.waitForPageLoad("Review Project Details");
        this.countinueButton.click();        
    }
    
}
export default new reviewProjectPage();