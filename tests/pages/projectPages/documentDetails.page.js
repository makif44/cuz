import Page from "../base.page";

class documentDetailsPage extends Page{

    get countinueButton(){return $("//div[@class='bot-nav-column']/button")};
    get documentType(){return $("//input[@placeholder='Document Type']")};
    get documentTypeList(){return $("#vs3__listbox")};
    get documentDetails(){return $("//input[@placeholder='Document Details']")};
    get documentDetailsList(){return $("#vs4__listbox")}
    
    selectDocumentType(arg1,arg2){
        this.documentType.waitForEnabled(5000);
        this.documentType.click();
        this.documentType.setValue(arg1);
        browser.pause(250);
        this.documentTypeList.click();
        this.documentDetails.click();
        this.documentDetails.setValue(arg2);
        browser.pause(250);
        this.documentDetailsList.click();
        // this.countinueButton.waitForDisplayed(5000);
        // this.countinueButton.click();
        
    }
}
export default new documentDetailsPage();