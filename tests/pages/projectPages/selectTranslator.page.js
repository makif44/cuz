import Page from "../base.page";

class selectTranslatorPage extends Page {

    get translator() {
        return $("(//div[@class='card selectable'])[1]")
    };

    selectTranslator() {
        this.waitForWrapperLoadMaskDisappear();
        this.translator.waitForExist({timeout:20000});
        browser.pause(1000)
        this.translator.click();
    }
}
export default new selectTranslatorPage();