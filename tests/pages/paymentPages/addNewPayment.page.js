import Page from "../base.page";
import card from '../../testData/validCardData'
import invalidCard from '../../testData/invalidCardData'
import addNewPayment from './addNewPayment.page'


class AddNewPayment extends Page {

    get cardHolderNameInbox() {
        return $("//input[@placeholder='Name']")
    };
    get cardNumberInbox() {
        return $("//input[@placeholder='________________']")
    };
    get expirationDateInbox() {
        return $("//input[@placeholder='MM/YY']")
    };
    get cvcumberInbox() {
        return $("//input[@placeholder='____']")
    };
    get countryInbox() {
        return $("//input[@class='vs__search']")
    };
    get postalCodeInbox() {
        return $("//input[@placeholder='Postal Code']")
    };
    get savePaymentButton() {
        return $("//button[@class='button w-button blue']")
    };
    get firstCountryInTheList() {
        return $("(//ul[contains(@id,'listbox')])[1]")
    }
    get cardHolderWarningField() {
        return $("div.section div.container.w-container span:nth-child(2) form.form span:nth-child(2) > p.error-detail.errors-block")
    }
    get cardNumberWarningField() {
        return $("div.section div.container.w-container span:nth-child(2) form.form span:nth-child(3) > p.error-detail.errors-block")
    }
    get expirationDateWarningField() {
        return $("div.section div.container.w-container span:nth-child(2) form.form span:nth-child(4) > p.error-detail.errors-block")
    }
    get cvcNumberWarningField() {
        return $("div.section div.container.w-container span:nth-child(2) form.form span:nth-child(5) > p.error-detail.errors-block")
    }
    get setAsADefaultButton() {
        return $("(//button[@class='button-alt w-button gray-alt -alt'])[1]")
    }
    get paymentMetodNumber() {
        return $$("//div[@class='indicator-column---noti-card']")
    };
    
    get deleteButton() {
        return $("(//div[@class='right-arrow-column---list-item'])[1]")
    }

    fillWithValidCardData(arg1) {
        let cardNum = '';
        let cardcvc = '';
        switch (arg1) {
            case 'Visa-Master':
                cardNum = card['visaCardNumber'];
                cardcvc = card['cvc'];
                break;
            case 'AmericanExp':
                cardNum = card['americaCardNumber'];
                cardcvc = card['a_cvc'];
                break;
            default:
                throw "Invalid card type!"
        }
        this.cardHolderNameInbox.setValue(card['cardName']);
        this.cardNumberInbox.setValue(cardNum);
        this.expirationDateInbox.setValue(card['expirationDate']);
        this.cvcumberInbox.setValue(cardcvc);
        this.postalCodeInbox.setValue(card['postalCode']);
        this.countryInbox.click();
        this.countryInbox.setValue(card['country']);
        //this.firstCountryInTheList.waitForDisplayed();
        browser.pause(500);
        this.firstCountryInTheList.click();
        //this.savePaymentButton.waitForClickable({timeout:5000,timeoutMsg:"Button not clickable"});
        browser.waitUntil(() =>
            this.savePaymentButton.isClickable(), {
                timeout: 10000,
                timeoutMsg: "Button not clickable"
            });
        this.savePaymentButton.click();
    }

    fillWithInvalidCardData() {
        this.cardHolderNameInbox.setValue(invalidCard[0]['cardName']);
        this.cardNumberInbox.setValue(invalidCard[0]['missingCardNumber']);
        this.expirationDateInbox.setValue(invalidCard[0]['expirationDate']);
        this.cvcumberInbox.setValue(invalidCard[0]['cvc']);
    }

    fillWithWrongCardNumber() {
        this.cardHolderNameInbox.setValue(card['cardName']);
        this.cardNumberInbox.setValue(card['wrongCardNumber']);
        this.expirationDateInbox.setValue(card['expirationDate']);
        this.cvcumberInbox.setValue(card['cvc']);
        this.postalCodeInbox.setValue(card['postalCode']);
        this.countryInbox.click();
        this.countryInbox.setValue(card['country']);
        //this.firstCountryInTheList.waitForEnabled();
        browser.pause(250);
        this.firstCountryInTheList.click();
        browser.waitUntil(() =>
            this.savePaymentButton.isClickable(), {
                timeout: 10000,
                timeoutMsg: "Button not clickable"
            });
        this.savePaymentButton.click();
    }

    clickSetAsADefault() {
        //browser.pause(5000);
        this. waitForWrapperLoadMaskDisappear();
        this.setAsADefaultButton.waitForClickable({
            timeout: 30000
        });
        this.setAsADefaultButton.click();
    }

    deletePaymentMethod() {  
        this.deleteButton.waitForEnabled({timeout:15000});
        this.deleteButton.click();
        this.confirmationYes.waitForDisplayed();
        this.confirmationYes.click();
    }

    verificationMessage(arg1, arg2) {
        var size = Object.keys(this.paymentMetodNumber).length;
        console.log('object size : ', size);
        browser.waitUntil(() => {
            return addNewPayment.greenWrapperNotification.getText() !== "Loading..."
        }, 5000, "notification not visible");
        if (size > 1) {
            addNewPayment.greenWrapperNotification.waitForDisplayed({
                timeout: 10000
            });
            expect(addNewPayment.greenWrapperNotification).toHaveText(arg1);
        } else {
            addNewPayment.redWrapperNotification.waitForDisplayed({
                timeout: 10000
            });
            expect(addNewPayment.redWrapperNotification).toHaveText(arg2);
        }
    }

}
export default new AddNewPayment();